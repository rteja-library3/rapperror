package rapperror_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/rteja-library3/rapperror"
)

func TestCreateErrForbidden(t *testing.T) {
	err := rapperror.ErrForbidden("", "", "", nil)

	assert.Error(t, err, "[TestCreateErrForbidden] Should error")
	assert.IsType(t, &rapperror.AppError{}, err, "[TestCreateErrForbidden] Should App error")
}

func TestCreateErrBadRequest(t *testing.T) {
	err := rapperror.ErrBadRequest("", "", "", nil)

	assert.Error(t, err, "[TestCreateErrBadRequest] Should error")
	assert.IsType(t, &rapperror.AppError{}, err, "[TestCreateErrBadRequest] Should App error")
}

func TestCreateErrNotFound(t *testing.T) {
	err := rapperror.ErrNotFound("", "", "", nil)

	assert.Error(t, err, "[TestCreateErrNotFound] Should error")
	assert.IsType(t, &rapperror.AppError{}, err, "[TestCreateErrNotFound] Should App error")
}

func TestCreateErrConflict(t *testing.T) {
	err := rapperror.ErrConflict("", "", "", nil)

	assert.Error(t, err, "[TestCreateErrConflict] Should error")
	assert.IsType(t, &rapperror.AppError{}, err, "[TestCreateErrConflict] Should App error")
}

func TestCreateErrUnauthorized(t *testing.T) {
	err := rapperror.ErrUnauthorized("", "", "", nil)

	assert.Error(t, err, "[TestCreateErrUnauthorized] Should error")
	assert.IsType(t, &rapperror.AppError{}, err, "[TestCreateErrUnauthorized] Should App error")
}

func TestCreateErrUnprocessableEntity(t *testing.T) {
	err := rapperror.ErrUnprocessableEntity("", "", "", nil)

	assert.Error(t, err, "[TestCreateErrUnprocessableEntity] Should error")
	assert.IsType(t, &rapperror.AppError{}, err, "[TestCreateErrUnprocessableEntity] Should App error")
}
