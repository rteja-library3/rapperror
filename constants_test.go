package rapperror_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/rteja-library3/rapperror"
)

func TestConstantsToString(t *testing.T) {
	s := rapperror.AppErrorCodeNotFound.String()

	assert.Equal(t, "err_not_found", s, "[TestConstantsToString] Should return \"err_not_found\"")
	assert.IsType(t, "", s, "[TestConstantsToString] return is should string")
}
