package rapperror_test

import (
	"fmt"
	"testing"

	"github.com/go-sql-driver/mysql"
	"github.com/stretchr/testify/assert"
	"gitlab.com/rteja-library3/rapperror"
)

func TestFromMysqlErrorInternalService(t *testing.T) {
	errx := fmt.Errorf("testing")

	err := rapperror.FromMysqlError(errx, "", "", nil)

	assert.Error(t, err, "[TestFromMysqlErrorInternalService] Should error")
	assert.Equal(t, rapperror.ErrInternalServerError(
		"",
		"",
		"",
		nil,
	), err, "[TestFromMysqlErrorInternalService] Should Internal Error")
}

func TestFromMysqlErrForbidden(t *testing.T) {
	errx := &mysql.MySQLError{
		Number: 1045,
	}

	err := rapperror.FromMysqlError(errx, "", "", nil)

	assert.Error(t, err, "[TestFromMysqlErrForbidden] Should error")
	assert.Equal(t, rapperror.ErrForbidden(
		rapperror.AppErrorCodeForbidden,
		"",
		"",
		nil,
	), err, "[TestFromMysqlErrForbidden] Should Error Forbidden")
}

func TestFromMysqlErrConflict(t *testing.T) {
	errx := &mysql.MySQLError{
		Number: 1062,
	}

	err := rapperror.FromMysqlError(errx, "", "", nil)

	assert.Error(t, err, "[TestFromMysqlErrConflict] Should error")
	assert.Equal(t, rapperror.ErrConflict(
		rapperror.AppErrorCodeDuplicate,
		"",
		"",
		nil,
	), err, "[TestFromMysqlErrConflict] Should Error Forbidden")
}

func TestFromMysqlErrNotFound(t *testing.T) {
	errx := &mysql.MySQLError{
		Number: 1452,
	}

	err := rapperror.FromMysqlError(errx, "", "", nil)

	assert.Error(t, err, "[TestFromMysqlErrNotFound] Should error")
	assert.Equal(t, rapperror.ErrNotFound(
		rapperror.AppErrorCodeNotFound,
		"",
		"",
		nil,
	), err, "[TestFromMysqlErrNotFound] Should Error Forbidden")
}
