package rapperror_test

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/rteja-library3/rapperror"
	"go.mongodb.org/mongo-driver/mongo"
)

func TestFromMongoNotFound(t *testing.T) {
	err := rapperror.FromMongoError(mongo.ErrNoDocuments, "", "", nil)

	assert.Error(t, err, "[TestFromMongoNotFound] Should error")
	assert.Equal(t, rapperror.ErrNotFound(
		rapperror.AppErrorCodeNotFound,
		"",
		"",
		nil,
	), err, "[TestFromMongoNotFound] Should Error Forbidden")
}

func TestFromMongoInternalserver(t *testing.T) {
	err := rapperror.FromMongoError(fmt.Errorf("error tes"), "", "", nil)

	assert.Error(t, err, "[TestFromMongoInternalserver] Should error")
	assert.Equal(t, rapperror.ErrInternalServerError(
		"",
		"",
		"",
		nil,
	), err, "[TestFromMongoInternalserver] Should Internal Error")
}

func TestFromMongoErrConflict(t *testing.T) {
	errx := mongo.CommandError{
		Code:    11000,
		Message: "Error Duplicate {tes}",
	}
	err := rapperror.FromMongoError(errx, "", "", nil)

	assert.Error(t, err, "[TestFromMongoErrConflict] Should error")
	assert.Equal(t, rapperror.ErrConflict(
		rapperror.AppErrorCodeDuplicate,
		"Duplicate {tes",
		"",
		nil,
	), err, "[TestFromMongoErrConflict] Should Internal Error")
}
