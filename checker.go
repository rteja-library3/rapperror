package rapperror

import "net/http"

func IsNotFoundError(err error) (b bool) {
	if e, ok := err.(*AppError); ok {
		return e.Status == http.StatusNotFound
	}

	return
}

func IsServerError(err error) (b bool) {
	if e, ok := err.(*AppError); ok {
		return e.Status == http.StatusInternalServerError
	}

	return
}
