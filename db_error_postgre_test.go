package rapperror_test

import (
	"fmt"
	"testing"

	"github.com/lib/pq"
	"github.com/stretchr/testify/assert"
	"gitlab.com/rteja-library3/rapperror"
)

func TestFromPostgreInternalserver(t *testing.T) {
	err := rapperror.FromPostgreError(fmt.Errorf("error tes"), "", "", nil)

	assert.Error(t, err, "[TestFromPostgreInternalserver] Should error")
	assert.Equal(t, rapperror.ErrInternalServerError(
		"",
		"",
		"",
		nil,
	), err, "[TestFromPostgreInternalserver] Should Internal Error")
}

func TestFromPostgreErrConflict(t *testing.T) {
	errx := &pq.Error{
		Code: "23505",
	}
	err := rapperror.FromPostgreError(errx, "", "", nil)

	assert.Error(t, err, "[TestFromPostgreErrConflict] Should error")
	assert.Equal(t, rapperror.ErrConflict(
		rapperror.AppErrorCodeDuplicate,
		"",
		"",
		nil,
	), err, "[TestFromPostgreErrConflict] Should Internal Error")
}
