package rapperror

import (
	"strings"

	"github.com/VividCortex/mysqlerr"
	"github.com/go-sql-driver/mysql"
	"github.com/lib/pq"
	"go.mongodb.org/mongo-driver/mongo"
)

func FromMysqlError(err error, msg, src string, data interface{}) error {
	switch v := err.(type) {
	case *mysql.MySQLError:
		switch v.Number {
		case mysqlerr.ER_ACCESS_DENIED_ERROR:
			return ErrForbidden(
				AppErrorCodeForbidden,
				msg,
				src,
				data,
			)
		case mysqlerr.ER_DUP_ENTRY, mysqlerr.ER_ROW_IS_REFERENCED_2:
			return ErrConflict(
				AppErrorCodeDuplicate,
				msg,
				src,
				data,
			)
		case mysqlerr.ER_NO_REFERENCED_ROW_2:
			return ErrNotFound(
				AppErrorCodeNotFound,
				msg,
				src,
				data,
			)
		}
	}

	return ErrInternalServerError(
		"",
		msg,
		src,
		data,
	)
}

func FromMongoError(err error, msg, src string, data interface{}) error {
	if err == mongo.ErrNoDocuments {
		return ErrNotFound(AppErrorCodeNotFound, msg, src, data)
	}

	if mongo.IsDuplicateKeyError(err) {
		e, ok := err.(mongo.ServerError)
		if ok {
			errMsg := e.Error()
			msg = "Duplicate " + errMsg[strings.LastIndex(errMsg, "{"):len(errMsg)-1]
		}

		return ErrConflict(AppErrorCodeDuplicate, msg, src, data)
	}

	return ErrInternalServerError(
		"",
		msg,
		src,
		data,
	)
}

func FromPostgreError(err error, msg, src string, data interface{}) error {
	switch v := err.(type) {
	case *pq.Error:
		switch v.Code.Name() {
		case "unique_violation":
			return ErrConflict(
				AppErrorCodeDuplicate,
				msg,
				src,
				data,
			)
		}
	}

	return ErrInternalServerError(
		"",
		msg,
		src,
		data,
	)
}
