package rapperror_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/rteja-library3/rapperror"
)

func TestErrorString(t *testing.T) {
	str := rapperror.ErrCommandIsRequired.Error()

	assert.Equal(t, "[err_internal_server] Command is required for this action", str, "[TestErrorString] return should \"[err_internal_server] Command is required for this action\"")
	assert.IsType(t, "", str, "[TestErrorString] return is should string")
}
