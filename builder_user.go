package rapperror

var (
	ErrUserInactive = ErrForbidden(
		AppErrorCodeInactiveUser,
		"Account inactive",
		"",
		nil,
	)
	ErrUserIncorrectPassword = ErrForbidden(
		AppErrorCodeIncorrectPassword,
		"Username or password incorect",
		"",
		nil,
	)
	ErrUserIncorrectUsername = ErrForbidden(
		AppErrorCodeIncorrectUsername,
		"Username or password incorect",
		"",
		nil,
	)
	ErrUserInvalidType = ErrForbidden(
		AppErrorCodeInvalidUserType,
		"Invalid account type",
		"",
		nil,
	)
	ErrPasswordRequired = ErrBadRequest(
		AppErrorCodePasswordRequired,
		"Password is required",
		"",
		nil,
	)
	ErrUserNotFound = ErrNotFound(
		AppErrorCodeUserNotFound,
		"User not found",
		"",
		nil,
	)
)
