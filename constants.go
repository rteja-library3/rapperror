package rapperror

type AppErrorCode string

func (o AppErrorCode) String() string {
	return string(o)
}

// default app error code
const (
	AppErrorCodeNotFound            AppErrorCode = "err_not_found"
	AppErrorCodeUnprocessableEntity AppErrorCode = "err_unprocessable_entity"
	AppErrorCodeUnauthorized        AppErrorCode = "err_unauthorized"
	AppErrorCodeInternalServerError AppErrorCode = "err_internal_server"
	AppErrorCodeDuplicate           AppErrorCode = "err_duplicate"
	AppErrorCodeBadRequest          AppErrorCode = "err_bad_request"
	AppErrorCodeForbidden           AppErrorCode = "err_forbidden"
)

// related to user / account
const (
	AppErrorCodeInactiveUser      AppErrorCode = "err_inactive_user"
	AppErrorCodeIncorrectPassword AppErrorCode = "err_incorrect_password"
	AppErrorCodeIncorrectUsername AppErrorCode = "err_incorrect_username"
	AppErrorCodeInvalidUserType   AppErrorCode = "err_invalid_user_type"
	AppErrorCodePasswordRequired  AppErrorCode = "err_password_required"
	AppErrorCodeUserNotFound      AppErrorCode = "err_user_not_found"
)

const (
	AppErrorCodeFieldRequired AppErrorCode = "err_field_required"
	AppErrorCodeInvalidJSON   AppErrorCode = "err_invalid_json"
)
