package rapperror_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/rteja-library3/rapperror"
)

func TestIsNotFoundError(t *testing.T) {
	err := rapperror.ErrDataNotFound

	b := rapperror.IsNotFoundError(err)
	assert.Equal(t, true, b, "[TestIsNotFoundError] Should return true")
}

func TestIsNotFoundErrorFalse(t *testing.T) {
	b := rapperror.IsNotFoundError(nil)
	assert.Equal(t, false, b, "[TestIsNotFoundErrorFalse] Should return false")
}

func TestIsServerError(t *testing.T) {
	err := rapperror.ErrInternalServerError("", "", "", nil)

	b := rapperror.IsServerError(err)
	assert.Equal(t, true, b, "[TestIsServerError] Should return true")
}

func TestTestIsServerErrorFalse(t *testing.T) {
	b := rapperror.IsServerError(nil)
	assert.Equal(t, false, b, "[TestTestIsServerErrorFalse] Should return false")
}
