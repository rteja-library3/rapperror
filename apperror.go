package rapperror

type AppError struct {
	Status  int
	Code    AppErrorCode
	Message string
	Source  string
	Data    interface{}
}

func (o AppError) Error() string {
	return "[" + o.Code.String() + "] " + o.Message
}
