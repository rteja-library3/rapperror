package rapperror

var (
	ErrFieldIsRequired = ErrBadRequest(
		AppErrorCodeFieldRequired,
		"Field is required",
		"",
		nil,
	)
	ErrDataDuplicate = ErrConflict(
		AppErrorCodeDuplicate,
		"Data is duplicate",
		"",
		nil,
	)
	ErrDataNotFound = ErrNotFound(
		AppErrorCodeNotFound,
		"Data is not found",
		"",
		nil,
	)
)
